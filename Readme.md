##Compile Sina# to C

####Tokenization (Lexical Analysis) :
~~~~
Main Expressions :

    Benevis("")^ -> printf("");
    Begir("%d",x)^ -> scanf("%d",&x);
    agar{} -> if()
    ta{} -> while()
    {} -> ()
    [] -> {}

example :
    agar{2 &B 3}[

    ]

Logical Expressions :

    &B -> >
    &BM -> >=
    &K -> <
    &KM -> <=
    &MM -> ==

Arithmetic Expressions :

    Jam -> +
    YekiBala -> ++
    Kam -> -
    YekiPain -> --
    Zarb -> *
    Tagsim -> /
    Bagimonde -> %

Variable Types :

    Sahih -> int
    Ashari -> float
    Harf -> char
~~~~

####token types:
~~~~
logical : 'logical',                                    > < <= >= ==
arithmetic : 'arithmetic',                              + - * / %
incDec : 'incDec',                                      ++ --
variableTypeInt : 'variableTypeInt',                    int
variableTypeFloat : 'variableTypeFloat',                float
variableTypeChar : 'variableTypeChar',                  char
string                                                  "whatever"
parOpens                                                (
parCloses                                               )
possibleVariableName : 'possibleVariableName',          start with (a-zA-Z) continue with (a-zA-Z0-9)*
possibleVariableNameRef : 'possibleVariableNameRef',    start with & then (a-zA-Z) continue with (a-zA-Z0-9)*
iFunction : 'iFunction',                                scanf
oFunction : 'oFunction',                                printf
conditionFunctions : 'conditionFunctions',              if while
int : 'int',                                            10
float : 'float',                                        10.2
char : 'char',                                          '.'
variableDelimiters : 'variableDelimiters',              =
separator : 'separator'                                 ,
ioFunctionDelimiters : 'ioFunctionDelimiters',          ;
bracketOpens : 'bracketOpens',                          {
bracketCloses : 'bracketCloses',                        }
curlyBracesOpens : 'curlyBracesOpens',                  (
curlyBracesCloses : 'curlyBracesCloses',                )
~~~~

####Parsing (Syntactical Analysis) Rules :
~~~~
Before logical : possibleVariableName,number,char,curlyBracesCloses
After Logical : possibleVariableName,number,char,curlyBracesOpens
Before arithmetic : possibleVariableName,number,curlyBracesCloses
After arithmetic : possibleVariableName,number,variableDelimiters,curlyBracesOpens
Before incDec : possibleVariableName,curlyBracesCloses
After incDec : ioFunctionDelimiters,curlyBracesCloses
Before variableTypes : ioFunctionDelimiters,bracketCloses,curlyBracesCloses(cast)
After variableTypes : possibleVariableName
Before possibleVariableName : variableDelimiters,ioFunctionDelimiters,bracketOpens,bracketCloses,curlyBracesOpens
After possibleVariableName : variableDelimiters,ioFunctionDelimiters,curlyBracesCloses
Before ioFunctions : ioFunctionDelimiters,bracketCloses
After ioFunctions : ioFunctionDelimiters
Before conditionFunctions : ioFunctionDelimiters,bracketCloses
After conditionFunctions : curlyBracesOpens
Before number : variableDelimiters,ioFunctionDelimiters,bracketOpens,curlyBracesOpens
After number : ioFunctionDelimiters,curlyBracesCloses
Before char : variableDelimiters,ioFunctionDelimiters,bracketOpens,curlyBracesOpens
After char : ioFunctionDelimiters,curlyBracesCloses
Before variableDelimiters : 
After variableDelimiters : 
Before ioFunctionDelimiters : bracketCloses,curlyBracesCloses
After ioFunctionDelimiters : bracketOpens,curlyBracesOpens
Before bracketOpens : 
After bracketOpens : bracketCloses,curlyBracesOpens
Before bracketCloses : curlyBracesCloses
After bracketCloses : curlyBracesOpens
Before curlyBracesOpens : 
After curlyBracesOpens : 
Before curlyBracesCloses : 
After curlyBracesCloses : 
~~~~
~~~~
Before Logical : possibleVariableName,number
After Logical : possibleVariableName,number
Before arithmetic : possibleVariableName,number
After arithmetic : possibleVariableName,number,variableDelimiters
Exceptions for arithmetic{
Before Bagimonde : possibleVariableName
After Bagimonde : possibleVariableName,number,
Before YekiBala : possibleVariableName
After YekiBala : ioFunctionDelimiters,bracketCloses,
Before YekiPain : possibleVariableName
After  YekiPain : ioFunctionDelimiters,bracketCloses,
}
Before variableTypes : bracketOpens,curlyBracesOpens
After variableTypes : possibleVariableName
Before possibleVariableName : variableTypes,arithmetic,Logical,variableDelimiters,ioFunctionDelimiters,bracketOpens
After possibleVariableName :arithmetic,Logical,variableDelimiters,ioFunctionDelimiters,curlyBracesCloses
Before ioFunctions :variableDelimiters,ioFunctionDelimiters,bracketOpens
After ioFunctions : ioFunctionDelimiters
Before conditionFunctions:bracketOpens,ioFunctionDelimiters,
After conditionFunctions : curlyBracesOpens
Before variableDelimiters :arithmetic,possibleVariableName,variableDelimiters,curlyBracesCloses
After variableDelimiters :variableDelimiters,number,string,possibleVariableName,curlyBracesOpens,ioFunctions
Before number :Logical,arithmetic,ioFunctionDelimiters,curlyBracesOpens
After number :Logical,arithmetic,curlyBracesCloses
Before string : curlyBracesOpens,variableDelimiters
After string  : curlyBracesCloses
Before ioFunctionDelimiters : possibleVariableName,ioFunctions,curlyBracesCloses
After ioFunctionDelimiters : possibleVariableName,number,ioFunctions,curlyBracesOpens,bracketCloses
Before bracketOpens : curlyBracesCloses,
After bracketOpens :ioFunctions,variableTypes,possibleVariableName
Before bracketCloses : ioFunctionDelimiters
After bracketCloses :
Before curlyBracesOpens : arithmetic,ioFunctions,conditionFunctions,ioFunctionDelimiters
After curlyBracesOpens : variableTypes,possibleVariableName,number,curlyBracesOpens,string
Before curlyBracesCloses : string,possibleVariableName,number
After curlyBracesCloses : ioFunctionDelimiters,bracketOpens,arithmetic,logical,variableDelimiters,bracketCloses
